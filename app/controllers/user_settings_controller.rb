class UserSettingsController < ApplicationController
  before_action :authenticate_user!
  def index
    # @users = User.where(id: current_user.id).page(params[:page] || 1).per(20)  
    @users = User.where.not(id: current_user.id).page(params[:page] || 1).per(20)  
  end

  def new
    @user = User.new()  
  end

  def edit
    @user = User.find(params[:id])
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to "/user_settings", notice: "User added"
    else
      render 'new'
    end
  end

  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      redirect_to "/user_settings", notice: "User details updated"
    else
      render 'edit'
    end
  end

  private
    def user_params
      params.require(:user).permit(:id, :email, :full_name, :password, :password_confirmation)
    end
end
