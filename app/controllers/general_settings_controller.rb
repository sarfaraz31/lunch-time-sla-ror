class GeneralSettingsController < ApplicationController
  before_action :authenticate_user!
  
  def index
    @general_settings = GeneralSetting.first
  end

  def new
    @general_settings = GeneralSetting.new                                                                 
  end

  def edit
    @general_settings = GeneralSetting.find(params[:id])
  end

  def create
    @general_settings = GeneralSetting.new(general_setting_params)
    if @general_settings.save
      redirect_to "/", notice: "General setings saved"
    else
      render 'new'
    end
  end

  def update
    @general_settings = GeneralSetting.find(params[:id])
    if @general_settings.update(general_setting_params)
      redirect_to "/", notice: "General setings updated"
    else
      render 'edit'
    end
  end

  def download_assets
    general_setting = GeneralSetting.first
    if general_setting.update_columns(assets_download_status: "downloading")
      DownloadAssetsJob.perform_later(general_setting.try(:format_domain), general_setting.try(:freshdesk_api_key), 1, Array.new)
    end
    redirect_to "/fs_assets"
  end
  
  private
  def general_setting_params
    params.require(:general_setting).permit(:id, :freshdesk_api_key, :freshdesk_domain)
  end
end
