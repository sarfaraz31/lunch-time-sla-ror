class CsvController < ApplicationController
  def index
  end

  def create
    # myfile = params[:uploaded_file]
    general_setting = GeneralSetting.first
    # csv_data = CSV.read(myfile.path)
    # csv_data = csv_data.drop(1)
    # puts csv_data.inspect
    # # AssetsSyncJob.perform_later(general_setting.try(:format_domain), general_setting.try(:freshdesk_api_key), 1, Array.new, csv_data)
    # UpdateAssetsJob.perform_later(general_setting.try(:format_domain), general_setting.try(:freshdesk_api_key), csv_data)
    # redirect_to "/csv"
    # # UpdateAssetsJob.perform_now(general_setting.try(:format_domain), general_setting.try(:freshdesk_api_key), csv_data)
    puts "~"*100
    Net::SFTP.start('5.32.110.4', 'ftpsap', :password => 'sap2FTP') do |sftp|
      # csv = sftp.download!("/home/ftpsap/uploads/SAP_FS.csv").split(/\r\n/)
      # csv.each do |line|
      #   puts line.inspect
      # end
      # sftp.file.open("/home/ftpsap/uploads/SAP_FS.csv", "r") do |f|
      #   puts f.gets.inspect
      # end
      # puts csv.inspect
      file_name = "ASSET_DATA_INPUT_#{Time.now.strftime("%Y%m%d")}.CSV"
      csv_data = sftp.download!("/home/ftpsap/#{file_name}").split(/\r\n/)
      csv_data = csv_data.drop(1) if csv_data.present?
      puts csv_data.inspect
      if csv_data.present?
        # AssetsSyncJob.perform_later(general_setting.try(:format_domain), general_setting.try(:freshdesk_api_key), 1, Array.new, csv_data)
        csv_data.each do |column|
          UpdateAssetsJob.set(queue: "update_assets".to_sym).perform_later(general_setting, general_setting.try(:format_domain), general_setting.try(:freshdesk_api_key), column)
        end
        
      end
      # sftp.upload!("app/assets/outputs/_output_05_08_2020.CSV", "/home/ftpsap/_output_05_08_2020.CSV")
    end
    puts "~"*100
  end
end
