class ApiController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :authenticate_user!, only: [:access_settings, :generate_api_key]
  before_action :verify_api_key, only: [:sync_ticket, :sync_setting]

  def sync_ticket
    if params[:data].present?
      checkTicketIfPresent
    end
    render :plain => {success:true}.to_json, status: 200, content_type: 'application/json'
  end

  def access_settings
    @general_settings = GeneralSetting.first
  end

  def generate_api_key
    payload = {time: Time.now.to_i, rand_num: Random.rand(9999999999999999) }
    puts payload
    token = JWT.encode payload, nil, 'none'
    general_settings = GeneralSetting.first
    if(general_settings.present?)
      general_settings.update(api_key: token)
    else
      GeneralSetting.create(api_key: token)
    end
    redirect_to "/access_settings", notice: "API key generated"
  end

  def checkTicketIfPresent
    params[:data] = JSON.parse(params[:data].to_json)
    if params[:data][:ticket].present?
      tickets = Ticket.where("fs_data ->> 'id' = '#{params[:data][:ticket][:id]}'")   
      tickets.present? ? updateTicket(tickets.first) : createTicket
    end
  end

  def createTicket
    status = getTicketStatus
    ticket = Ticket.create(fs_data: params[:data][:ticket], current_status_id: status.try(:id))
  end

  def updateTicket(ticket)
    status = getTicketStatus
    ticket.update(fs_data: params[:data][:ticket], current_status_id: status.id )
  end

  def getTicketStatus
    return Status.find_by(fs_value: params[:data][:ticket][:status].to_i)
  end

  def sync_setting
    if params.present?
      checkGroupSettingPresent
    end
    render :plain => {success:true}.to_json, status: 200, content_type: 'application/json'
  end
  
  def checkGroupSettingPresent
    params[:group_settings].each do |key, group|
      group_setting = GroupSetting.find_by(group_id: group[:group_id]);
      group_setting.present? ? updateSetting(group_setting, group) : createGroupSetting(group)
    end
  end

  def createGroupSetting(group)
    group = GroupSetting.create(group_id: group[:group_id], start_time: group[:start_time], end_time: group[:end_time], time_zone: group[:time_zone])
  end

  def updateSetting(group_setting, group)
    puts "~"*50
    puts group[:time_zone]
    group_setting.update(group_id: group[:group_id], start_time: group[:start_time], end_time: group[:end_time], time_zone: group[:time_zone])
  end

  def verify_api_key
    api_key = GeneralSetting.try(:first).try(:api_key)
    if (params[:iparams].present?)
      data = JSON.parse(params[:iparams].to_json)
      checkApiKey(data["db"]["middleware_apikey"], api_key)
    elsif (params.present?)
      data = JSON.parse(params.to_json)
      checkApiKey(data["middleware_apikey"], api_key)
    else 
      render :plain => {sucess:false}.to_json, status: 401, content_type: 'application/json'
    end
  end

  def checkApiKey (middleware_apiKey, api_key)
    if(middleware_apiKey!=api_key)
      render :plain => {success:false}.to_json, status: 401, content_type: 'application/json'
    end
  end

end
