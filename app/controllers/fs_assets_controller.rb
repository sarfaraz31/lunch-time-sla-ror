class FsAssetsController < ApplicationController
  def index
    @general_settings = GeneralSetting.first
  end
  
  def upload
    general_setting = GeneralSetting.first
    csv_data = CSV.read(params[:file].path)
    if csv_data.present?
      general_setting.update_columns(assets_upload_status: "uploading")
      UploadAssetsJob.set(queue: "update_assets".to_sym).perform_later(general_setting, csv_data)
      redirect_to "/fs_assets", notice: "Assets are updating..."
    else
      redirect_to "/fs_assets", error: "No data found in the CSV file"
    end
  end

  def sync
    general_setting = GeneralSetting.first
    general_setting.update_columns(assets_sync_status: "syncing")
    AssetsSyncJob.set(queue: "assets_sync".to_sym).perform_later(general_setting.try(:format_domain), general_setting.try(:freshdesk_api_key), 1, Array.new)
    SyncVendorsJob.set(queue: "vendors_sync".to_sym).perform_later(general_setting.try(:format_domain), general_setting.try(:freshdesk_api_key), 1)
    redirect_to "/fs_assets", notice: "Assets are syncing..."
  end
end
