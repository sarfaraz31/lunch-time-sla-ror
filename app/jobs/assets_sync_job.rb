class AssetsSyncJob < ApplicationJob
  queue_as :default

  def perform (domain_name, api_key, page, assets)
    response = HTTParty.get("#{domain_name}/api/v2/assets?per_page=30&page=#{page}", headers: {Authorization: "Basic " + Base64.encode64("#{api_key}:x")})
    if (response.present? && response.parsed_response.present? || response.code==200)
      Delayed::Worker.logger.debug response
      response = response.parsed_response["assets"]
      Delayed::Worker.logger.debug response
      if response.count === 30
        AssetsSyncJob.set(queue: "assets_sync".to_sym).perform_later(domain_name, api_key, page + 1, assets.concat(response))
      else
        assetData = assets.concat(response)
        assetData = assetData.flatten
        assetData.each do |asset|
          getAssetDetails(asset, domain_name, api_key)
        end
        # UpdateAssetsJob.set(queue: "update_assets".to_sym).perform_now(domain_name, api_key, csv_data)
      end
    else
      Delayed::Worker.logger.debug("Sync assets - API response - #{response.code}")
      Delayed::Worker.logger.debug(response)
      AssetsSyncJob.set(queue: "assets_sync".to_sym).perform_later(domain_name, api_key, page, assets)
    end
  end

  def getAssetDetails(asset, domain_name, api_key)
    AssetDetailsJob.set(queue: "assets_sync".to_sym).perform_later(asset, domain_name, api_key, "assets_sync")
  end  
end

