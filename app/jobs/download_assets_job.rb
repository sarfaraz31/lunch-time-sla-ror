class DownloadAssetsJob < ApplicationJob
  queue_as :default

  def perform (domain_name, api_key, page, assets)
    response = HTTParty.get("#{domain_name}/api/v2/assets?per_page=30&page=#{page}", headers: {Authorization: "Basic " + Base64.encode64("#{api_key}:x")})
    if (response.present? && response.parsed_response.present? || response.code==200)
      Delayed::Worker.logger.debug response
      response = response.parsed_response["assets"]
      Delayed::Worker.logger.debug response
      if response.count === 30
        DownloadAssetsJob.set(queue: "assets_download".to_sym).perform_later(domain_name, api_key, page + 1, assets.concat(response))
      else
        assetData = assets.concat(response)
        assetData.each do |asset|
          getAssetDetails(asset, domain_name, api_key)
        end
        # UpdateAssetsJob.set(queue: "update_assets".to_sym).perform_now(domain_name, api_key, csv_data)
      end
    else
      Delayed::Worker.logger.debug("Download assets - API response - #{response.code}")
      Delayed::Worker.logger.debug(response)
      DownloadAssetsJob.set(queue: "assets_download".to_sym).perform_later(domain_name, api_key, page, assets)
    end
  end

  def getAssetDetails(asset, domain_name, api_key)
    AssetDetailsJob.set(queue: "asset_details_download".to_sym).perform_later(asset, domain_name, api_key, "asset_details_download")

    # response = HTTParty.get("#{domain_name}/api/v2/assets/#{asset["display_id"]}?include=type_fields", headers: {Authorization: "Basic " + Base64.encode64("#{api_key}:x")})
    # if (response.present? && response.parsed_response.present? || response.code==200)
    #   @asset_id = response["asset"]["id"]
    #   @asset = Asset.find_by(asset_id: @asset_id)
    #   fields = response["asset"]["type_fields"]
    #   fields.each do |key, value|
    #     if(key.start_with?("company_code"))
    #       @company_code = value
    #     end
    #     if(key.start_with?("asset_code"))
    #       @asset_code = value
    #     end
    #     if(key.start_with?("serial_number"))
    #       @serial_number = value
    #     end
    #     if(key.start_with?("employee_code"))
    #       @employee_code = value
    #     end
    #   end
    #   @asset.present? ? updateAsset(response["asset"]) : createAsset(response["asset"]) 
    # else 
    #   puts "Faild to fetch asset Details"
    # end
  end

  def createAsset(asset)
    Asset.create(asset_id: @asset_id, company_code: @company_code, employee_code: @employee_code, serial_number: @serial_number, asset_code: @asset_code, asset_data: asset)
  end

  def updateAsset(payload)
    @asset.update(asset_id: @asset_id, company_code: @company_code, employee_code: @employee_code, serial_number: @serial_number, asset_code: @asset_code, asset_data: payload)
  end
end
