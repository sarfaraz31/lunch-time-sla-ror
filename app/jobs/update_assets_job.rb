class UpdateAssetsJob < ApplicationJob
  queue_as :default

  def perform(general_setting, domain_name, api_key, column)
    # Do something later
    @general_setting = general_setting
    if column.present?
      column = column.split(",") if !column.kind_of?(Array)
      asset = Asset.find_by(asset_code: column[0].to_s, company_code: column[1].to_s)
      if (asset.present? && asset.asset_data.present? && asset.asset_data["type_fields"].present?)
        type_field_keys = asset.asset_data["type_fields"].keys
        asset_code_field_name = type_field_keys.grep(/asset_code/).try(:first)
        company_code_field_name = type_field_keys.grep(/company_code/).try(:first)
        purchase_order_value_field_name = type_field_keys.grep(/cost/).try(:first)
        invoice_number_field_name = type_field_keys.grep(/invoice_number/).try(:first)
        invoice_date_field_name = type_field_keys.grep(/acquisition_date/).try(:first)
        purchase_order_field_name = type_field_keys.grep(/purchase_order/).try(:first)
        # vendor_name_field_name = type_field_keys.grep(/vendor/).try(:first)
        vendor_field_name = type_field_keys.grep(/seller_name/).try(:first)
        cost_centre_field_name = type_field_keys.grep(/cost_center/).try(:first)

        headers = {
          "Authorization" => "Basic " + Base64.encode64("#{api_key}:x"),
          "Content-Type" => "application/json"
        }
        type_fields = {}
        type_fields[asset_code_field_name.to_sym] = column[0].to_s if (asset_code_field_name.present? && column[0].present?)
        type_fields[company_code_field_name.to_sym] = column[1].to_s if (company_code_field_name.present? && column[1].present?)
        type_fields[purchase_order_value_field_name.to_sym] = column[3].to_f if (purchase_order_value_field_name.present? && column[3].present?)
        type_fields[invoice_number_field_name.to_sym] = column[4].to_s if (invoice_number_field_name.present? && column[4].present?)
        begin
          type_fields[invoice_date_field_name.to_sym] = column[5].try(:to_datetime).try(:iso8601) if (invoice_date_field_name.present? && column[5].present?)
        rescue
          type_fields[invoice_date_field_name.to_sym] = nil
        end
        type_fields[purchase_order_field_name.to_sym] = column[6].to_s if (purchase_order_field_name.present? && column[6].present?)
        type_fields[cost_centre_field_name.to_sym] = column[8].to_s if (cost_centre_field_name.present? && column[8].present?)

        type_fields[vendor_field_name.to_sym] = column[7].to_s if (vendor_field_name.present? && column[7].present?)

        # if column[7].present?
        #   vendor = Vendor.find_by(name: column[7].to_s.gsub("\u0000", ''))
        #   if vendor.present?
        #     type_fields[vendor_name_field_name.to_sym] = vendor.fs_id.to_i if vendor_name_field_name.present?
        #   else
        #     response = HTTParty.post("#{domain_name}/api/v2/vendors", 
        #     body: { 
        #       name: column[7].to_s}.to_json,
        #     headers: {
        #       "Authorization" => "Basic " + Base64.encode64("#{api_key}:x"),
        #       "Content-Type" => "application/json"
        #     })
        #     Delayed::Worker.logger.debug "creating vendor"
        #     Delayed::Worker.logger.debug response.code
        #     Delayed::Worker.logger.debug response
        #     if response.code == 200
        #       vendor_data = JSON.parse response.body
        #       vendor = Vendor.create(fs_id: vendor_data["id"].to_s.gsub("\u0000", ''), name: vendor_data["name"].to_s.gsub("\u0000", '')) if (vendor_data["name"].present? && vendor_data["id"].present?)
        #       type_fields[vendor_name_field_name.to_sym] = vendor.fs_id.to_i if vendor_name_field_name.present?
        #     elsif response.code != 409
        #       UpdateAssetsJob.set(queue: "update_assets".to_sym).perform_later(general_setting, domain_name, api_key, column)
        #     end
        #   end
        #   # type_fields[vendor_name_field_name.to_sym] = vendor.fs_id if (vendor_name_field_name.present? && vendor.present?)
        # end
        
        # body = {
        #   asset_tag: column[1].to_s+column[0].to_s
        # }
        body = {}
        # body[:user_id] = column[2].to_i if ((column[2].present?) && (column[2].to_i!=0))
        body[:type_fields] = type_fields
        puts body
        response = HTTParty.put("#{domain_name}/api/v2/assets/#{asset.asset_data["display_id"]}", headers: headers, body: body.to_json)
        puts "~"*100
        Delayed::Worker.logger.debug "updating asset"
        Delayed::Worker.logger.debug response.code
        Delayed::Worker.logger.debug response
        Delayed::Worker.logger.debug(response)
        if response.code == 200
          puts response["asset"].to_json
          asset.update(asset_data: response["asset"])
          # file_name = "#{Time.now.strftime("%d_%m_%Y")}.json"
          # dir_path = "app/assets/fs_updates/#{file_name}"
          # jsonAsset = {asset_code: column[1], company_code: column[0], employee_code: column[7], serial_number: column[5]}
          # File.open(dir_path,"a+") do |f|
          #   f.write(", "+jsonAsset.to_json)
          # end
        elsif response.code!=400
          UpdateAssetsJob.set(queue: "update_assets".to_sym).perform_later(general_setting, domain_name, api_key, column)
        end
        puts "~"*100
      end
    end
  end
end
