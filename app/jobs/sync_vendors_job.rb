class SyncVendorsJob < ApplicationJob
  queue_as :default

  def perform (domain_name, api_key, page)
    response = HTTParty.get("#{domain_name}/api/v2/vendors?per_page=100&page=#{page}", headers: {Authorization: "Basic " + Base64.encode64("#{api_key}:x")})
    if (response.present? && response.parsed_response.present? || response.code==200)
      Delayed::Worker.logger.debug response
      response = response.parsed_response["vendors"]
      Delayed::Worker.logger.debug response
      if response.count === 100
        SyncVendorsJob.set(queue: "vendors_sync".to_sym).perform_later(domain_name, api_key, page + 1)
      end
      check_vendors(response)
    else
      Delayed::Worker.logger.debug("Sync Vendors - API response - #{response.code}")
      Delayed::Worker.logger.debug(response)
      SyncVendorsJob.set(queue: "vendors_sync".to_sym).perform_later(domain_name, api_key, page)
    end
  end

  def check_vendors(vendors)
    vendors.each do |vendor_data|
      if (vendor_data["name"].present? && vendor_data["id"].present?)
        vendor = Vendor.find_by(fs_id: vendor_data["id"].to_s)
        if !vendor.present?
          Vendor.create(fs_id: vendor_data["id"].to_s.gsub("\u0000", ''), name: vendor_data["name"].to_s.gsub("\u0000", ''))
        else
          vendor.update(name: vendor_data["name"].to_s.gsub("\u0000", ''))
        end
      end
    end
  end
end
