class AssetDetailsJob < ApplicationJob
  queue_as :default

  def perform (asset, domain_name, api_key, queue_name)
    response = HTTParty.get("#{domain_name}/api/v2/assets/#{asset["display_id"]}?include=type_fields", headers: {Authorization: "Basic " + Base64.encode64("#{api_key}:x")})
    Delayed::Worker.logger.debug("Response code for getting asset details - #{response.code}")
    Delayed::Worker.logger.debug("Response from getting asset details - #{response}")
    if (response.present? && response.parsed_response.present? || response.code==200)
      @asset_id = response["asset"]["id"]
      @asset = Asset.find_by(asset_id: @asset_id)
      fields = response["asset"]["type_fields"]
      @asset_code = response['asset']['asset_tag']
      if @asset_code.present?
        @company_code = @asset_code[0, 4]
        @asset_code = @asset_code[4..]
      end
      user_id = response['asset']['user_id']
      asset = response['asset']
      if user_id.present?
        response = HTTParty.get("#{domain_name}/api/v2/requesters/#{user_id}", headers: {Authorization: "Basic " + Base64.encode64("#{api_key}:x")})
        if (response.present? && response.parsed_response.present? || response.code==200)
          if (response["requester"].present? && response["requester"]["custom_fields"].present?)
            @employee_code = response["requester"]["custom_fields"]["employee_id"]
          end
        elsif (response.present? && response.code==404)
          response = HTTParty.get("#{domain_name}/api/v2/agents/#{user_id}", headers: {Authorization: "Basic " + Base64.encode64("#{api_key}:x")})
          if (response.present? && response.parsed_response.present? || response.code==200)
            if (response["agent"].present? && response["agent"]["custom_fields"].present?)
              @employee_code = response["agent"]["custom_fields"]["employee_id"]
            end
          elsif response.code!=404
            Delayed::Worker.logger.debug("Response from employee id - #{response.code}")
            AssetDetailsJob.set(queue: queue_name.to_sym).perform_later(asset, domain_name, api_key)  
          end
        else
          Delayed::Worker.logger.debug("Response from employee id - #{response.code}")
          AssetDetailsJob.set(queue: queue_name.to_sym).perform_later(asset, domain_name, api_key)
        end
      end
      @employee_code = "00000000" if !@employee_code.present?
      fields.each do |key, value|
        # if(key.start_with?("asset_code"))
        #   @asset_code = value
        #   if @asset_code.present?
        #     @company_code = @asset_code[0, 4]
        #     @asset_code = @asset_code[4..]
        #   end
        # end
        if(key.start_with?("serial_number"))
          @serial_number = value
        end
      end
      @asset.present? ? updateAsset(asset) : createAsset(asset) 
    else 
      puts "Faild to fetch asset Details"
      Delayed::Worker.logger.debug("Download asset details - API response - #{response.code}")
      AssetDetailsJob.set(queue: queue_name.to_sym).perform_later(asset, domain_name, api_key)
    end
  end

  def createAsset(asset)
    Asset.create(asset_id: @asset_id, company_code: @company_code, employee_code: @employee_code, serial_number: @serial_number, asset_code: @asset_code, asset_data: asset)
  end

  def updateAsset(payload)
    @asset.update(asset_id: @asset_id, company_code: @company_code, employee_code: @employee_code, serial_number: @serial_number, asset_code: @asset_code, asset_data: payload)
  end
end
