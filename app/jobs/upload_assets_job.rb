class UploadAssetsJob < ApplicationJob
  queue_as :default

  def perform(general_setting, csv_data)
    csv_data.each do |row|
      UpdateAssetsJob.set(queue: "update_assets".to_sym).perform_later(general_setting, general_setting.try(:format_domain), general_setting.try(:freshdesk_api_key), row)
    end
    # Do something later
  end
end
