class GeneralSetting < ApplicationRecord
  enum assets_download_status: [:yet_to_start, :downloading, :completed]
  enum assets_upload_status: [:not_yet_started, :uploading, :uploaded]
  enum assets_sync_status: [:not_synced, :syncing, :synced]
  validate :fd_api_key_is_valid
  after_save :store_field_values

  def fd_api_key_is_valid
    if freshdesk_api_key.present? && freshdesk_domain.present?
      response = HTTParty.get("#{self.format_domain}/api/v2/ticket_fields", headers: {Authorization: "Basic " + Base64.encode64("#{freshdesk_api_key}:x")})
      if ((!response.present? || !response.parsed_response.present?) || response.code!=200)
        errors.add(:freshdesk_api_key, "Freshservice API key is invalid")
      end
    elsif !freshdesk_domain.present?
      errors.add(:freshdesk_domain, "Please fill Freshservice domain")
    elsif !freshdesk_api_key.present?
      errors.add(:freshdesk_api_key, "Please fill Freshservice API key")
    end
  end

  def store_field_values
    puts "~"*50
    puts saved_change_to_api_key?
    if (!saved_change_to_api_key?)
      response = HTTParty.get("#{self.format_domain}/api/v2/ticket_fields", headers: {Authorization: "Basic " + Base64.encode64("#{freshdesk_api_key}:x")})
      if (response.present? && response.parsed_response.present? || response.code==200)
        status_field = response.parsed_response["ticket_fields"].detect {|f| f["name"] == 'status'}
        checkAndUpdateStatuses(status_field)
        # AssetsSyncJob.perform_later(self.format_domain, freshdesk_api_key, 1, Array.new)
      end
    end
  end

  def checkAndUpdateStatuses(status_field)
    status_field["choices"].each do |key, value|
      status = Status.find_by(fs_value: key.to_i)
      if !status.present?
        Status.create(fs_value: key.to_i, name: value.first)
      end
    end
  end

  def format_domain
    return ((self.freshdesk_domain.to_s.include? "https://") ? self.freshdesk_domain : "https://#{self.freshdesk_domain}")
  end

  def check_all_updated
    pending = Delayed::Job.where("queue=?", "update_assets")
    if(!pending.present? || pending.count<=5)
      sleep 5
      # self.generateCSV
    end
  end

  def generateCSV
    json_data = self.getRecordsFromJSON
    if json_data.present?
      csv_data = []
      headers = ['Asset Code', 'Company Code', 'Employee Code', 'Serial Number']
      csv_data << headers
      json_data.each do |hash|
        csv_data << hash.values
      end
      puts csv_data.inspect
      file_name = "ASSET_DATA_OUTPUT_#{Time.now.strftime("%Y%m%d")}.csv"
      file_path = "app/assets/outputs/#{file_name}"
      File.write(file_path, csv_data.map(&:to_csv).join)
      sleep 2
      Net::SFTP.start('5.32.110.4', 'ftpsap', :password => 'sap2FTP') do |sftp|
        sftp.upload!(file_path, "/home/ftpsap/#{file_name}")
      end
    end
  end

  def getRecordsFromJSON
    file_name = "#{Time.now.strftime("%d_%m_%Y")}.json"
    file_path = "app/assets/fs_updates/#{file_name}"
    if File.exist?(file_path)
      file = File.read(file_path)
      if file.present?
        file.slice!(0)
        file.slice!(0)
        file = "["+file+"]"
        return JSON.parse(file)
      else
        return []
      end
    else
      return []
    end
  end
end
