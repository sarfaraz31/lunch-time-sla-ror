Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root "general_settings#index"
  post '/api/sync_ticket', to: "api#sync_ticket"
  post '/api/sync_setting', to: "api#sync_setting" 
  get '/access_settings', to: "api#access_settings"
  post '/generate/api_key', to: "api#generate_api_key"

  resources :general_settings do
    get "download_assets", on: :collection
  end
  resources :user_settings
  resources :csv
  resources :fs_assets do
    post "upload", on: :collection
    get "sync", on: :collection
  end
  resources :service_requests do
    get "download", on: :collection
    get "file_not_found", on: :collection
  end
end
