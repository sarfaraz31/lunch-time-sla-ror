# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
set :output, "log/cron.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

every 1.minute do
  rake 'tickets:update_status'
  rake 'check_assets_download_status'
  rake 'check_assets_upload_status'
  rake 'check_assets_sync_status'
end

# every 0 6 * * * do
#   rake 'assets:update_assets'
# end

every 1.day, at: '12:00 am' do
  rake 'assets:sync_from_sftp'
end

every 1.day, at: '1:00 am' do
  rake 'assets:update_assets'
end

every 1.day, at: '3:00 am' do
  rake 'assets:sync_to_sftp'
end

# every 1.hour do
#   rake 'assets:update_assets'
# end

# every 2.hours do
#   rake 'assets:sync_to_sftp'
# end

every '0 8-18 * * 0-5' do
  rake 'hourly_customer_export'
end

# In this updated code, the cron expression '0 8-18 * * 0-5' is used to define the schedule. Let's break down the updated cron expression:

# Minutes field: 0
# The 0 means the task will run at the 0th minute of each hour.
# Hours field: 8-18
# The 8-18 means the task will run between the 8th and 18th hour (8 AM to 6 PM).
# Days of the month field: *
# The * means the task will run on any day of the month.
# Months field: *
# The * means the task will run on any month.
# Days of the week field: 0-5
# The 0-5 means the task will run from Sunday (0) to Friday (5).