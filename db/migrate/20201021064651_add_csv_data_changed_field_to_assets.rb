class AddCsvDataChangedFieldToAssets < ActiveRecord::Migration[6.0]
  def change
    add_column :assets, :csv_data_changed, :boolean, default: false
    #Ex:- add_column("admin_users", "username", :string, :limit =>25, :after => "email")
  end
end
