class CreateGeneralSettings < ActiveRecord::Migration[6.0]
  def change
    create_table :general_settings do |t|
      t.string :freshdesk_api_key
      t.string :freshdesk_domain
      t.text :api_key
      
      t.timestamps
    end
  end
end
