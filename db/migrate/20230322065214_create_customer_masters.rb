class CreateCustomerMasters < ActiveRecord::Migration[6.0]
  def change
    create_table :customer_masters do |t|
      t.string :ref_code
      t.string :ref_value
      t.string :master_type
      
      t.timestamps
    end
  end
end
