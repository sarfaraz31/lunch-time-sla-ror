class CreateGroupSettings < ActiveRecord::Migration[6.0]
  def change
    create_table :group_settings do |t|
      t.integer :group_id, limit: 8 
      t.time :start_time
      t.time :end_time
      t.string :time_zone
      t.timestamps
    end
  end
end
