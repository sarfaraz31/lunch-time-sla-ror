class CreateTickets < ActiveRecord::Migration[6.0]
  def change
    create_table :tickets do |t|
      t.json :fs_data
      t.integer :current_status_id
      t.timestamps
    end
  end
end

