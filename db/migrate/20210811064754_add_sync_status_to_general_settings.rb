class AddSyncStatusToGeneralSettings < ActiveRecord::Migration[6.0]
  def change
    add_column :general_settings, :assets_sync_status, :integer, :default => 0
  end
end
