class ChangeColumnTypeForAssets < ActiveRecord::Migration[6.0]
  def change
    change_column :assets, :asset_code, :string
    change_column :assets, :company_code, :string
    change_column :assets, :employee_code, :string
    change_column :assets, :serial_number, :string
  end
end
