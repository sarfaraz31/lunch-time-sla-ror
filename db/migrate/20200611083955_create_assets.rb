class CreateAssets < ActiveRecord::Migration[6.0]
  def change
    create_table :assets do |t|
      t.integer :asset_id, limit: 8 
      t.integer :asset_code, limit: 8 
      t.integer :company_code, limit: 8 
      t.integer :employee_code, limit: 8 
      t.integer :serial_number, limit: 8 
      t.json :asset_data
      t.timestamps
    end
  end
end
