class AddAssetUploadStatusToGeneralSettings < ActiveRecord::Migration[6.0]
  def change
    add_column :general_settings, :assets_upload_status, :integer, :default => 0
  end
end
