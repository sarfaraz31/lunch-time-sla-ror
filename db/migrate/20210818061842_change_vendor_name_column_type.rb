class ChangeVendorNameColumnType < ActiveRecord::Migration[6.0]
  def change
    change_column :vendors, :name, :text
  end
end
