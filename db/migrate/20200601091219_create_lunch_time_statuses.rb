class CreateLunchTimeStatuses < ActiveRecord::Migration[6.0]
  def change
    create_table :lunch_time_statuses do |t|
      t.integer :ticket_id, limit: 8
      t.integer :group_id, limit: 8
      t.integer :status_id
      t.timestamps
    end
  end
end
