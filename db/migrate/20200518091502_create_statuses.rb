class CreateStatuses < ActiveRecord::Migration[6.0]
  def change
    create_table :statuses do |t|
      t.string :name
      t.integer :fs_value
      t.timestamps
    end
  end
end
