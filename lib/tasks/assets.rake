namespace :assets do
  desc "Update assets to database"
  task update_assets: :environment do
    puts "Cron is running !!!"
    date = Date.today.strftime("%Y-%m-%d")  
    puts date
    updated_assets = fetch_assets('?query="updated_at:%27'+ date + '%27"&page=', 1, Array.new)
    puts updated_assets.count
    updated_assets.each do |asset|
      puts asset["display_id"]
      update_database(asset["display_id"])
    end
  end
end

def update_database(id)
  general_settings = GeneralSetting.first
  url = "#{general_settings.format_domain}/api/v2/assets/#{id}?include=type_fields"
  response = HTTParty.get(url,
    headers: {
      "Authorization" => "Basic " + Base64.encode64("#{general_settings.freshdesk_api_key}:x"),
      "Content-Type" => "application/json"
    })
  if (response.present? && response.parsed_response.present? || response.code==200)
    @asset_id = response["asset"]["id"]
    @asset = Asset.find_by(asset_id: @asset_id)
    fields = response["asset"]["type_fields"]
    @asset_code = response['asset']['asset_tag']
    if (@asset_code.present? && (@asset_code.length >= 10))
      @company_code = @asset_code[0, 4]
      @asset_code = @asset_code[4..]
    end
    user_id = response['asset']['user_id']
    asset = response['asset']
    if user_id.present?
      response = HTTParty.get("#{general_settings.format_domain}/api/v2/requesters/#{user_id}", headers: {Authorization: "Basic " + Base64.encode64("#{general_settings.freshdesk_api_key}:x")})
      if (response.present? && response.parsed_response.present? || response.code==200)
        if (response["requester"].present? && response["requester"]["custom_fields"].present?)
          @employee_code = response["requester"]["custom_fields"]["employee_id"]
        end
      elsif (response.present? && response.code==404)
        response = HTTParty.get("#{general_settings.format_domain}/api/v2/agents/#{user_id}", headers: {Authorization: "Basic " + Base64.encode64("#{general_settings.freshdesk_api_key}:x")})
        if (response.present? && response.parsed_response.present? || response.code==200)
          if (response["agent"].present? && response["agent"]["custom_fields"].present?)
            @employee_code = response["agent"]["custom_fields"]["employee_id"]
          end
        end
      end
    end
    @employee_code = "00000000" if !@employee_code.present?
    fields.each do |key, value|
      if(key.start_with?("serial_number"))
        @serial_number = value
      end
    end
    @asset.present? ? updateAsset(response["asset"]) : createAsset(response["asset"]) 
  else 
    puts "Faild to fetch asset Details"
  end
end

def createAsset(asset)
  Asset.create(asset_id: @asset_id, company_code: @company_code, employee_code: @employee_code, serial_number: @serial_number, asset_code: @asset_code, asset_data: asset, csv_data_changed: true)
end

def updateAsset(payload)
  if((@asset.company_code != @company_code) || (@asset.employee_code != @employee_code) ||  (@asset.serial_number != @serial_number))
    @asset.update(asset_id: @asset_id, company_code: @company_code, employee_code: @employee_code, serial_number: @serial_number, asset_code: @asset_code, asset_data: payload, csv_data_changed: true)
  else
    @asset.update(asset_id: @asset_id, company_code: @company_code, employee_code: @employee_code, serial_number: @serial_number, asset_code: @asset_code, asset_data: payload)
  end
end

def fetch_assets(query, page, assets)
  general_settings = GeneralSetting.first
  url = "#{general_settings.format_domain}/api/v2/assets#{query}#{page}"
  response = HTTParty.get(url,
    headers: {
      "Authorization" => "Basic " + Base64.encode64("#{general_settings.freshdesk_api_key}:x"),
      "Content-Type" => "application/json"
    })
  puts '#'*50
  puts page
  puts response.code
  puts '#'*50
  if (response.present? && response.parsed_response.present? || response.code==200)
    response = response.parsed_response["assets"]
    if response.count === 30
      fetch_assets(query, page + 1, assets.concat(response))
    else
      assetData = assets.concat(response)
      return assetData
    end
  end
end
