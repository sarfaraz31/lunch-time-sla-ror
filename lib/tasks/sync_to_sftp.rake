namespace :assets do
  desc "Sync updated assets to SFTP"
  task sync_to_sftp: :environment do
    puts "Sync updated assets to SFTP"
    @assets = Asset.where("updated_at::date = ?", Date.today).where(csv_data_changed: true)
    if @assets.present?
      @file_name = "ASSET_DATA_OUTPUT_#{Time.now.strftime("%Y%m%d%H%M")}.csv"
      @file_path = "app/assets/outputs/#{@file_name}"
      generate_csv
      upload_csv
    end
  end
end

task sync_from_sftp: :environment do
  puts "Sync updated assets from SFTP"
  general_setting = GeneralSetting.first
  Net::SFTP.start('5.32.110.4', 'ftpsap', :password => 'sap2FTP') do |sftp|
    file_name = "ASSET_DATA_INPUT_#{Time.now.strftime("%Y%m%d")}.CSV"
    csv_data = sftp.download!("/home/ftpsap/#{file_name}").split(/\r\n/)
    csv_data = csv_data.drop(1) if csv_data.present?
    puts csv_data.inspect
    if csv_data.present?
      csv_data.each do |column|
        UpdateAssetsJob.set(queue: "update_assets".to_sym).perform_later(general_setting, general_setting.try(:format_domain), general_setting.try(:freshdesk_api_key), column)
      end
    end
  end
end

task check_assets_download_status: :environment do
  pending = Delayed::Job.where("queue=? OR queue=?", "assets_download", "asset_details_download")
  general_setting = GeneralSetting.first
  if (!pending.present? && general_setting.assets_download_status=="downloading")
    general_setting = GeneralSetting.first
    general_setting.update_columns(assets_download_status: "completed")
    @assets = Asset.all
    if @assets.present?
      @file_name = "ASSET_DATA_#{Time.now.strftime("%Y%m%d%H%M")}.csv"
      @file_path = "app/assets/outputs/#{@file_name}"
      generate_csv
      upload_csv
    end
  end
end

task check_assets_upload_status: :environment do
  pending = Delayed::Job.where("queue=?", "update_assets")
  general_setting = GeneralSetting.first
  if (!pending.present? && general_setting.assets_upload_status=="uploading")
    general_setting.update_columns(assets_upload_status: "uploaded")
  end
end

task check_assets_sync_status: :environment do
  pending = Delayed::Job.where("queue=? OR queue=?", "assets_sync", "vendors_sync")
  general_setting = GeneralSetting.first
  if (!pending.present? && general_setting.assets_sync_status=="syncing")
    general_setting.update_columns(assets_sync_status: "synced")
  end
end

def generate_csv
  if @assets.present?
    csv_data = []
    headers = ['Company Code', 'Asset Code', 'Employee Code', 'Serial Number']
    csv_data << headers
    @assets.each do |asset|
      if (asset.asset_code.present? || asset.company_code.present? || asset.employee_code.present? || asset.serial_number.present?)
        csv_values = []
        csv_values << asset.company_code.to_s
        csv_values << asset.asset_code.to_s
        csv_values << asset.employee_code.to_s
        csv_values << asset.serial_number.to_s
        csv_data << csv_values
      end
    end
    puts csv_data.inspect
    File.write(@file_path, csv_data.map(&:to_csv).join)
    @assets.update_all(csv_data_changed: false)
  end
end

def upload_csv
  Net::SFTP.start('5.32.110.4', 'ftpsap', :password => 'sap2FTP') do |sftp|
    sftp.upload!(@file_path, "/home/ftpsap/#{@file_name}")
  end
end
