namespace :tickets do
  desc "Update tickets with lunch time Status"
  task update_status: :environment do
    group_settings = GroupSetting.all
    group_settings.each do |group|
      zone = ActiveSupport::TimeZone.new(group[:time_zone])
      current_time =Time.now.in_time_zone(zone).strftime("%H:%M")
      start_time = group[:start_time].strftime("%H:%M")
      end_time = group[:end_time].strftime("%H:%M")
      if(current_time == start_time)
        changeStatusToLunchBreak(group)
      elsif (current_time == end_time)
        changeToPreviousStatus(group)
      end
    end
  end
end

def changeStatusToLunchBreak(group)
  tickets =  Ticket.where("fs_data ->> 'group_id' = '#{group[:group_id]}'")   
  tickets.each do |ticket|
    sla_ticket = LunchTimeStatus.where(ticket_id: ticket.fs_data["id"])
    statuses = Status.where("name = 'Lunch Break'")
    if sla_ticket.present?
      sla_ticket.update(group_id: ticket.fs_data["group_id"], status_id: ticket.fs_data["status"])
      update_sla(ticket, statuses.first.fs_value)
    else
      LunchTimeStatus.create(ticket_id: ticket.fs_data["id"], group_id: ticket.fs_data["group_id"], status_id: ticket.fs_data["status"])
      update_sla(ticket, statuses.first.fs_value)
    end
  end
end

def changeToPreviousStatus(group)
  groups_list = LunchTimeStatus.where(group_id: group[:group_id]);
  groups_list.each do |group_status|
    ticket = Ticket.where("fs_data ->> 'id' = '#{group_status[:ticket_id]}'")
    update_sla(ticket.first, group_status[:status_id])
  end
end

def update_sla(ticket, status)
  general_settings = GeneralSetting.first
  url = "#{general_settings.format_domain}/api/v2/tickets/#{ticket.fs_data["id"]}"
  response = HTTParty.put(url, body: { status: status }.to_json,
    headers: {
      "Authorization" => "Basic " + Base64.encode64("#{general_settings.freshdesk_api_key}:x"),
      "Content-Type" => "application/json"
    })
  # puts response
  # update_sla(custom_fields, ticket) if response.code!=200
end