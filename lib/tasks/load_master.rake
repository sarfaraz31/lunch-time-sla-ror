task :load_master => :environment do
  puts Time.now.strftime("%m-%d-%Y, %I:%M %p")
  puts "Importing customer master..."
  workbook = Roo::Spreadsheet.open 'public/integration_for_customer_master.xlsx'
  worksheets = workbook.sheets
  
  valid_master_types = ["Business Partner", "Business Partner Category", "Title", "Company Code", "SaleesOrg", "TermsOfPayment", "Country Key", "Region", "Division", "DeliveryPlant", "TaxKD"]
  
  worksheets.each do |worksheet|
    if valid_master_types.include? worksheet.to_s
      num_rows = 0
      if worksheet.to_s != "SaleesOrg"
        master_type = worksheet.to_s.split(" ").join("").underscore
      else
        master_type = "sales_org"
      end
      workbook.sheet(worksheet).each_row_streaming do |row|
        if num_rows > 0
          row_cells = row.map { |cell| cell.value }
          customer_master = CustomerMaster.find_by(master_type: master_type, ref_value: row_cells[1].to_s)
          if !customer_master.present?
            CustomerMaster.create(master_type: master_type, ref_value: row_cells[1].to_s, ref_code: row_cells[0].to_s)
          end
        end
        num_rows += 1
      end
      # puts "Read #{num_rows} rows" 
    end
  end
  puts "Customer master imported!"
end