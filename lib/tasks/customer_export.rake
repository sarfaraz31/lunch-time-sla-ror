task hourly_customer_export: :environment do
  @general_settings = GeneralSetting.first
  @filtered_service_requests = []
  @csv_data = []
  puts "Downloading service requests..."
  download_sr(1)
  puts "Filtering service requests..."
  @filtered_service_requests = @filtered_service_requests.flatten
  download_requester_details if @filtered_service_requests.present?
  download_requested_items if @filtered_service_requests.present?
  # format_csv_data if @csv_data.present?
  date = DateTime.now.strftime("%d%m%y_%H%M")
  @file_name = "CUSTOMER_OUT_#{date}.csv"
  puts "#{@csv_data.count} data found"
  export_csv if @csv_data.present?
  upload_to_sftp if @csv_data.present?
end


def download_sr(page)
  # date = DateTime.now.utc.strftime("%Y-%m-%d")
  # date = 1.days.ago.utc.strftime("%Y-%m-%d")
  # url = "/api/v2/tickets?type=Service+Request&updated_since=#{date}T00:00:00Z&per_page=100&page=#{page}"
  date = 1.hour.ago.utc.strftime("%Y-%m-%dT%H:%M:00Z")
  url = "/api/v2/tickets?type=Service+Request&updated_since=#{date}&per_page=100&page=#{page}"
  response = request_api(url)
  if response["tickets"].present?
    filter_sr(response["tickets"])
    download_sr(page+1) if (response["tickets"].count == 100)
  end
end

def filter_sr(tickets)
  if tickets.present?
    # @filtered_service_requests << tickets.select{|sr| (sr["updated_at"].to_date == Date.yesterday) && (sr["category"].present? && sr["category"].downcase == "sap")}
    @filtered_service_requests << tickets.select{|sr| (sr["updated_at"].to_datetime.utc.to_date == DateTime.now.utc.to_date)}
    # @filtered_service_requests << tickets.select{|sr| (sr["updated_at"].to_datetime.utc.to_date == 1.days.ago.utc.to_date)}
  end
end

def download_requester_details
  puts "Downloading requester details for filtered service requests..."
  @filtered_service_requests = @filtered_service_requests.each do |sr|
    url = "/api/v2/requesters/#{sr["requester_id"]}"
    response = request_api(url)
    if response["requester"].present?
      sr["requester_details"] = response["requester"]
    end
  end
end

def download_requested_items
  puts "Downloading requested items for filtered service requests..."
  @filtered_service_requests.each do |sr|
    url = "/api/v2/tickets/#{sr["id"]}/requested_items"
    response = request_api(url)
    if response["requested_items"].present?
      #57 is for SAP customer code
      # puts response["requested_items"].inspect
      filtered_items = response["requested_items"].select{|req_item| (req_item["service_item_id"] == 57)} 
      if filtered_items.present?
        sr["requested_items"] = filtered_items.first
        @csv_data << sr
        # customer_items = filtered_items.select{|item| (item["custom_fields"]["party_type"] == "Customer")}
        # vendor_items = filtered_items.select{|item| (item["custom_fields"]["party_type"] == "Vendor")}
        # if customer_items.present? && sr["updated_at"].to_date == Date.yesterday
        #   sr["requested_items"] = customer_items.first
        #   @csv_data << sr
        # elsif vendor_items.present? && (sr["approval_status"].present? && sr["approval_status"] == 1)
        #   db_sr = ServiceRequest.find_by(fs_id: sr["id"])
        #   if !db_sr.present?
        #     ServiceRequest.create(fs_id: sr["id"], approved_at: Time.now)
        #     sr["requested_items"] = vendor_items.first
        #     @csv_data << sr
        #   end
        # end
      end
    end
  end
end

def request_api(url)
  response = HTTParty.get("#{@general_settings.format_domain}/#{url}", headers: {Authorization: "Basic " + Base64.encode64("#{@general_settings.freshdesk_api_key}:x")})
  return JSON.parse(response.body)
end

def format_csv_data
  if @csv_data.present?
    puts "Formatting CSV..."
    formatted_sr = []
    @csv_data.each do |sr|
      custom_fields_data = sr.try(:[], "requested_items").try(:[], "custom_fields")
      formatted_data = {
        ticket_number: sr.try(:[], "id"),
        run_id: "",
        business_partner_grouping: get_ref_code("business_partner", custom_fields_data.try(:[], "business_partner_grouping")),
        business_partner_category: get_ref_code("business_partner_category", custom_fields_data.try(:[], "business_partner_category")),
        title: get_ref_code("title", custom_fields_data.try(:[], "title")),
        first_name: custom_fields_data.try(:[], "first_name"),
        name_2: "",
        last_name: custom_fields_data.try(:[], "last_name"),
        search_term_1_for_bp: custom_fields_data.try(:[], "first_name"),
        search_term_2_for_bp: custom_fields_data.try(:[], "last_name"),
        street: custom_fields_data.try(:[], "address_and_street_name"),
        postal_code: custom_fields_data.try(:[], "postal_code"),
        city: custom_fields_data.try(:[], "city"),
        country_key: get_ref_code("country_key", custom_fields_data.try(:[], "country")),
        region: get_ref_code("region", custom_fields_data.try(:[], "region")),
        telephone_no: custom_fields_data.try(:[], "telephone_number"),
        telephone_no_extension: "",
        mobile_number: custom_fields_data.try(:[], "mobile_number"),
        fax_number: "",
        fax_number_extension: "",
        email_address: custom_fields_data.try(:[], "email_address"),
        address_time_zone: "",
        vendor: "",
        bank_country_key: "",
        bank_keys: "",
        bank_account_number: "",
        account_holder_name: "",
        company_code: get_ref_code("company_code", custom_fields_data.try(:[], "company_code")),
        customer_account_group: "",
        recon_account: "",
        sort_key: "0001",
        previous_account_no: "",
        terms_of_payment_key: get_ref_code("terms_of_payment", custom_fields_data.try(:[], "terms_of_payment")),
        credit_memo_pyt_term: "",
        tolerance_group: "",
        record_pmnt_history: "",
        payment_methods: "",
        dunning_procedure: "",
        account_statement: "",
        sales_org: get_sales_org(custom_fields_data),
        distribution_channel: "01",
        division: get_ref_code("division", custom_fields_data.try(:[], "division")),
        sales_district: "",
        customer_group: "",
        account_at_customer: "",
        currency: custom_fields_data.try(:[], "currency"),
        order_probability: "",
        customer_price_group: "",
        cust_pric_procedure: 1,
        price_list_type: "",
        customer_statistics_group: "",
        delivery_priority: "",
        order_combination_indicator: "X",
        shipping_condition: "01",
        delivering_plant: get_ref_code("company_code", custom_fields_data.try(:[], "company_code")),
        max_part_deliveries: 9,
        incoterms_part_1: "",
        incoterms_part_2: "",
        terms_of_payment_key_1: get_ref_code("terms_of_payment", custom_fields_data.try(:[], "terms_of_payment")),
        credit_control_area: get_ref_code("company_code", custom_fields_data.try(:[], "company_code")),
        account_assignment_gp: "Z1",
        taxkd: get_ref_code("tax_kd", custom_fields_data.try(:[], "taxkd")),
        customer_group_1: "",
        user_name: get_user_name(sr),
        user_email_id: sr.try(:[], "requester_details").try(:[], "primary_email"),
        emirates_id: custom_fields_data.try(:[], "emirates_id"),
        trade_license_number: custom_fields_data.try(:[], "trade_license_number"),
        trn_number: custom_fields_data.try(:[], "trn_number"),
        name_of_the_contact_person: custom_fields_data.try(:[], "name_of_the_contact_person")
      }
      formatted_sr << formatted_data
    end
    @csv_data = formatted_sr
  end
end

def get_sales_org sr
  sales_org = (sr.try(:[], "sales_org_ad01") || sr.try(:[], "sales_org_dx01") || sr.try(:[], "sales_org_bd01") || sr.try(:[], "sales_org_ho01") || sr.try(:[], "sales_org_misc") || sr.try(:[], "sales_org_kw01") || sr.try(:[], "sales_org_md01") || sr.try(:[], "sales_org_om01") || sr.try(:[], "sales_org_qt01"))
  return get_ref_code("sales_org", sales_org)
end

def get_user_name sr
  sr.try(:[], "requester_details").try(:[], "first_name") + " " + sr.try(:[], "requester_details").try(:[], "last_name")
end

def get_ref_code master_type, ref_value
  CustomerMaster.find_by(master_type: master_type, ref_value: ref_value).try(:ref_code)
end

def export_csv
  puts "Generating CSV..."
  file_path = "public/#{@file_name}"
  json_to_csv(file_path)
end

def json_to_csv(file_path)
  headers = ["Ticket Number", "Run ID", "Business Partner Grouping", "Business Partner Category", "Title", "Name1/First Name", "Name2/Last Name", "Last Name", "Search term 1 for BP", "Search term 2 for BP", "Street", "City postal code", "City", "Country Key", "Region", "Telephone no", "Telephone no: Extension", "Mobile No", "Fax number", "Fax no: Extension", "E-Mail Address", "Address time zone", "Vendor", "Bank country key", "Bank Keys", "Bank account number", "Account Holder Name", "Company Code", "Customer Account Group", "Recon. Account", "Sort key", "Previous account no.", "Terms of Payment Key", "Credit memo pyt term", "Tolerance group", "Record pmnt history", "Payment methods", "Dunning Procedure", "Account statement", "Sales Org.", "Distribution Channel", "Division", "Sales District", "Customer Group", "Account at customer", "Currency", "Order Probability", "Customer Price Group", "Cust.pric.procedure", "Price list type", "Customer Statistics Group", "Delivery Priority", "Order Combination Indicator", "Shipping Condition", "Delivering Plant", "Max.Part.Deliveries", "Incoterms (Part 1)", "Incoterms (Part 2)", "Terms of Payment Key", "Credit control area", "Account AssignmentGp", "TAXKD", "Customer group 1", "User Name", "User Email ID", "Emirates ID", "Trade License Number", "TRN Number", "Name Of The Contact Person"] 
  CSV.open(file_path, "w", :write_headers=> true, :headers => headers) do |csv|
    @csv_data.each { |item| csv << item.values }
  end
  puts "CSV generated!"
end 

def upload_to_sftp
  Net::SFTP.start('5.32.110.4', 'ftpsap-2', :password => 'sap2FTP') do |sftp|
    sftp.upload!("#{Rails.root}/public/#{@file_name}", "/home/ftpsap-2/INBOUND/#{@file_name}")
  end
end